from pydriller import RepositoryMining
import re
import csv

preParamMap = {}
postParamMap = {}

def findParams(paramSet):
    paramList = [x.strip() for x in paramSet.split(",")]
    return paramList

def ckIfModifiedAccordingToContraintes(preParamSet, postParamSet):
    preParamList = findParams(preParamSet)
    postParamList = findParams(postParamSet)

    if(not len(postParamList) == len(preParamList) + 1):
        return False
    misMatch = 0
    preParamMap = {}
    for param in preParamList:
        preParamMap[param] = True

    for param in postParamList:
        if not(param in preParamMap):
            misMatch = misMatch + 1

    return misMatch == 1

csvCandidates = []

for commit in RepositoryMining('https://github.com/springfox/springfox').traverse_commits():
    for mod in commit.modifications:
        if(mod.change_type != mod.change_type.MODIFY):
            continue
        if(mod.filename[-5:] != ".java"):
            continue

        matchesInPreviousCode = re.findall("(protected|private|static|public)[' '].*[' '](.*)\((.*)\)", mod.source_code_before)
        matchesInCode = re.findall("(protected|private|static|public)[' '].*[' '](.*)\((.*)\)", mod.source_code)

        for element in matchesInPreviousCode:
            preParamMap[element[1]] = element[2]

        for element in matchesInCode:
            postParamMap[element[1]] = element[2]

        for func in preParamMap:
            if func in postParamMap:
                if ckIfModifiedAccordingToContraintes(preParamMap[func], postParamMap[func]):
                    csvCandidates.append((commit.hash, mod.filename, func, preParamMap[func], postParamMap[func]))

with open('repoMining.csv', 'w') as file:
    writer = csv.writer(file, quoting=csv.QUOTE_ALL)
    writer.writerow(["Commit SHA", "File Name", "Old function signature", "New function signature"])
    for element in csvCandidates:
        writer.writerow([element[0], element[1], element[2] + "(" + element[3]+")", element[2] + "(" + element[4]+")"])





